﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InsertPage.aspx.cs" Inherits="Sol_Checkbox_Task.InsertPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="updatePanel" runat="server">
            <ContentTemplate>
                <table>

                    <tr>
                        <td>
                            <asp:TextBox ID="txtFirstName" runat="server" placeholder="FirstName"></asp:TextBox>
                        </td> 
                    </tr>

                    <tr>
                        <td>
                            <asp:TextBox ID="txtLastName" runat="server" placeholder="LastName"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                         <td>
                            <asp:CheckBox ID="chkOnSite" runat="server" Text="ON SITE" Checked="false" OnCheckedChanged="chkOnSite_CheckedChanged" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                        </td>
                    </tr>

                     <tr>
                        <td>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </td>
                    </tr>   
                    
                </table>

            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
