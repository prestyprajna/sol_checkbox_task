﻿CREATE PROCEDURE uspGetUser
(
	@Command VARCHAR(MAX),
	@UserId NUMERIC (18,0),
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Onsite BIT,

	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
)
AS

	BEGIN 

	--declaration
	DECLARE @ErrorMessage VARCHAR(MAX)

		IF @Command='SEARCH_BY_ID'
		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY

				SELECT U.FirstName,
				U.LastName,
				U.OnSite
					FROM tblUser AS U
						WHERE U.UserId=@UserId

				SET @Status=1
				SET @Message='SEARCH SUCCESSFULL'
				COMMIT TRANSACTION

			END TRY

			BEGIN CATCH
				SET @ErrorMessage=ERROR_MESSAGE()

				SET @Status=0
				SET @Message='SEARCH EXCEPTION'

				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)
			END CATCH

		END

		ELSE IF @Command='SELECT_ALL'
		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY

				SELECT U.UserId,
				U.FirstName,
				U.LastName,
				U.OnSite
					FROM tblUser AS U
						

				SET @Status=1
				SET @Message='SELECT SUCCESSFULL'
				COMMIT TRANSACTION

			END TRY

			BEGIN CATCH
				SET @ErrorMessage=ERROR_MESSAGE()

				SET @Status=0
				SET @Message='SELECT EXCEPTION'

				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)
			END CATCH

		END


	END

