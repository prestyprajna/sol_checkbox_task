﻿using Sol_Checkbox_Task.DAL;
using Sol_Checkbox_Task.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Checkbox_Task
{
    public partial class UpdatePage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(IsPostBack==false)
            {
                this.BindUserIdData();
            }

        }

        protected void btnBind_Click(object sender, EventArgs e)
        {
            txtFirstName.Visible = true;
            txtLastName.Visible = true;
            //ddlUserID.Enabled = true;

            UserEntity userEntityObj = new UserEntity()
            {
                UserId =Convert.ToDecimal(ddlUserID.SelectedValue)
            };


            UserEntity userEntityObj1 = new UserDal().SearchData(userEntityObj);

            txtFirstName.Text = userEntityObj1.FirstName;
            txtLastName.Text = userEntityObj1.LastName;
            chkOnSite.Checked = Convert.ToBoolean(userEntityObj1.OnSite);


        }

        protected void chkOnSite_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void BindUserIdData()  //bind userId data to drop down list
        {
            var getUserId = new UserDal().SelectData(null);

            ddlUserID.DataSource = getUserId;
            ddlUserID.DataBind();

            ddlUserID.AppendDataBoundItems = true;
            ddlUserID.Items.Insert(0, new ListItem("--Select UserId--", "0"));
            ddlUserID.SelectedIndex = 0;
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            
            UserEntity userEntityObj = new UserEntity()
            {
                UserId=Convert.ToDecimal(ddlUserID.SelectedItem.Value),
                FirstName = txtFirstName.Text,
                LastName = txtLastName.Text,
                OnSite = chkOnSite.Checked
            };           
            

            var message = new UserDal().EditUserData(userEntityObj);

            lblMessage.Text = message;
        }
    }
}