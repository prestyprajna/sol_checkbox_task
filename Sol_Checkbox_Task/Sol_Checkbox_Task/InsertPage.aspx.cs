﻿using Sol_Checkbox_Task.DAL;
using Sol_Checkbox_Task.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Checkbox_Task
{
    public partial class InsertPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            UserEntity userEntityObj = new UserEntity()
            {
                FirstName = txtFirstName.Text,
                LastName = txtLastName.Text,
                OnSite = Convert.ToBoolean(chkOnSite.Checked)
            };

            var message = new UserDal().AddUserData(userEntityObj);

            lblMessage.Text = message;
        }

        protected void chkOnSite_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}