﻿CREATE TABLE [dbo].[tblCountry] (
    [CountryId]   NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [CountryName] VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([CountryId] ASC)
);

