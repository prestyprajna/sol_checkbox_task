﻿CREATE PROCEDURE uspGetState
(
	@Command VARCHAR(MAX),

	@StateId NUMERIC(18,0),
	@StateName VARCHAR(50),
	@CountryId NUMERIC(18,0),

	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
)
AS
	BEGIN

	--declaration
	DECLARE @ErrorMessage VARCHAR(MAX)

		IF @Command='GetStateData'
		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY

				SELECT S.StateId,
				S.StateName,
				S.CountryId 
					FROM tblState AS S
						WHERE S.CountryId=@CountryId

				SET @Status=1
				SET @Message='GetStateDataSuccessfull'

				COMMIT TRANSACTION
				
			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()

				SET @Status=0
				SET @Message='GetStateDataException'


				ROLLBACK TRANSACTION

				RAISERROR(@ErrorMessage,16,1)

			END CATCH
		END

	END
