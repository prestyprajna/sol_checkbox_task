﻿using Sol_Checkbox_Task.DAL.ORD;
using Sol_Checkbox_Task.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Checkbox_Task.DAL
{
    public class UserDal
    {
        #region  declaration
        private UserDCDataContext _dc = null;
        #endregion

        #region  constructor

        public UserDal()
        {
            _dc = new UserDCDataContext();
        }

        #endregion

        #region   public methods

        public string AddUserData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;

            var setQuery=_dc?.uspSetUser(
                "INSERT",
                userEntityObj?.UserId,
                userEntityObj?.FirstName,
                userEntityObj?.LastName,
                userEntityObj?.OnSite,
                ref status,
                ref message
                );

            return message;
        }

        public string EditUserData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;

            var setQuery = _dc?.uspSetUser(
                "UPDATE",
                userEntityObj?.UserId,
                userEntityObj?.FirstName,
                userEntityObj?.LastName,
                userEntityObj?.OnSite,
                ref status,
                ref message
                );

            return message;
        }


        public UserEntity SearchData(UserEntity userEntityObj)  //search data by id
        {
            int? status = null;
            string message = null;

            var getQuery = _dc?.uspGetUser(
                "SEARCH_BY_ID",
                userEntityObj?.UserId,
                userEntityObj?.FirstName,
                userEntityObj?.LastName,
                userEntityObj?.OnSite,
                ref status,
                ref message
                )
                ?.AsEnumerable()
                ?.Select((leUserObj) => new UserEntity()
                {
                    FirstName = leUserObj?.FirstName,
                    LastName = leUserObj?.LastName,
                    OnSite = leUserObj?.OnSite
                })
                ?.FirstOrDefault();

            return getQuery;
        }

        public List<UserEntity> SelectData(UserEntity userEntityObj)   //select all user data
        {
            int? status = null;
            string message = null;

            var getQuery = _dc?.uspGetUser(
                "SELECT_ALL",
                userEntityObj?.UserId,
                userEntityObj?.FirstName,
                userEntityObj?.LastName,
                userEntityObj?.OnSite,
                ref status,
                ref message
                )
                ?.AsEnumerable()
                ?.Select((leUserObj) => new UserEntity()
                {
                    UserId=leUserObj?.UserId
                    //FirstName = leUserObj?.FirstName,
                    //LastName = leUserObj?.LastName,
                    //OnSite = leUserObj?.OnSite
                })
                ?.ToList();

            return getQuery;
        }


        #endregion
    }
}