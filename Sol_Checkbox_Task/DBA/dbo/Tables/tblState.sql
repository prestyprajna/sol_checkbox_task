﻿CREATE TABLE [dbo].[tblState] (
    [StateId]   NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [StateName] VARCHAR (50) NULL,
    [CountryId] NUMERIC (18) NOT NULL,
    PRIMARY KEY CLUSTERED ([StateId] ASC)
);

