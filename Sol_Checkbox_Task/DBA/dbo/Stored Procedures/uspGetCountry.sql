﻿CREATE PROCEDURE uspGetCountry
(
	@Command VARCHAR(MAX),

	@CountryId NUMERIC(18,0),
	@CountryName VARCHAR(50),

	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
)
AS
	BEGIN

	--declaration
	DECLARE @ErrorMessage VARCHAR(MAX)

		IF @Command='GetCountryData'
		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY

				SELECT Co.CountryId,
				Co.CountryName				
					FROM tblCountry AS Co
						--WHERE S.CountryId=@CountryId

				SET @Status=1
				SET @Message='GetCountryDataSuccessfull'

				COMMIT TRANSACTION
				
			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()

				SET @Status=0
				SET @Message='GetCountryDataException'


				ROLLBACK TRANSACTION

				RAISERROR(@ErrorMessage,16,1)

			END CATCH
		END

	END
