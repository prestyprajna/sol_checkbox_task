﻿CREATE TABLE [dbo].[tblCity] (
    [CityId]   NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [CityName] VARCHAR (50) NULL,
    [StateId]  NUMERIC (18) NOT NULL,
    PRIMARY KEY CLUSTERED ([CityId] ASC)
);

