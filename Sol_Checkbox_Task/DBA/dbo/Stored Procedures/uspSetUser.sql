﻿CREATE PROCEDURE uspSetUser
(
	@Command VARCHAR(MAX),
	@UserId NUMERIC (18,0),
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Onsite BIT,

	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
)
AS

	BEGIN 

	--declaration
	DECLARE @ErrorMessage VARCHAR(MAX)

	IF @Command='INSERT'
	BEGIN

		BEGIN TRANSACTION

		BEGIN TRY

			INSERT INTO tblUser
			(
				FirstName,
				LastName,
				OnSite
			)
			VALUES 
			(
				@FirstName,
				@LastName,
				@Onsite
			)


			SET @Status=1
			SET @Message='INSERT SUCCESSFULL'
			COMMIT TRANSACTION

		END TRY

		BEGIN CATCH
			SET @ErrorMessage=ERROR_MESSAGE()

			SET @Status=0
			SET @Message='INSERT EXCEPTION'

			ROLLBACK TRANSACTION
			RAISERROR(@ErrorMessage,16,1)
		END CATCH

	END

	ELSE IF @Command='UPDATE'
	BEGIN

		BEGIN TRANSACTION

		BEGIN TRY

			SELECT 
			@FirstName=CASE WHEN @FirstName IS NULL THEN U.FirstName ELSE @FirstName END,
			@LastName=CASE WHEN @LastName IS NULL THEN U.FirstName ELSE @LastName END,
			@Onsite=CASE WHEN @Onsite IS NULL THEN U.OnSite ELSE @Onsite END
				FROM tblUser AS U

			UPDATE tblUser
				SET FirstName=@FirstName,
				LastName=@LastName,
				OnSite=@Onsite
					WHERE UserId=@UserId

			SET @Status=1
			SET @Message='UPDATE SUCCESSFULL'
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH

			SET @ErrorMessage=ERROR_MESSAGE()

			SET @Status=0
			SET @Message='UPDATE EXCEPTION'

			ROLLBACK TRANSACTION
			RAISERROR(@ErrorMessage,16,1)

		

		END CATCH

	END


	END

